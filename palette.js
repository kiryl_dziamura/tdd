var Palette = function () {
	this.toRgb = function (hex) {
		var hexColor, re, hexArray, rgb;
		re = /#([\d, a, b, c, d, e, f, A, B, C, D, E, F]{6})/;
		if (!hex.match(re)) throw new ReferenceError('bad format of string');
		else {
			if (hex.match(re)[0] != hex) throw new ReferenceError('bad format of string');
			hexArray = hex.match(re)[1].match(/.{2}/g);
			rgb = 'rgb(';
			for (var i = 0; i < hexArray.length; i++) {
				rgb += parseInt(hexArray[i], 16).toString();
				if (i!=hexArray.length-1) rgb += ', ';
			};
			rgb+=')';
		}
		return rgb;
	}

	this.toHex = function (rgb) {
		var rgbColor, re, array, partOfColor, hex;
		re = /rgb\(\s*(\d{1,3})\s*\,\s*(\d{1,3})\s*\,\s*(\d{1,3})\s*\)/;
		array = rgb.match(re);
		if (!array) throw new ReferenceError('bad format of string');
		else {
			if (rgb.match(re)[0] != rgb) throw new ReferenceError('bad format of string');
			hex = '#';
			for (var i = 1; i < array.length; i++) {
				if (parseInt(array[i]) > 255) throw new ReferenceError('part of color is more then 255');
				partOfColor = parseInt(array[i]).toString(16);
				if (partOfColor.length==1) partOfColor='0'+partOfColor;
				hex += partOfColor;
			};
		}
		return hex;
	}
};

