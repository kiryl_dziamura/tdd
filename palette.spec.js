describe('Palette', function () {
	describe('#toRgb', function () {
		it('returns rgb parts of color', function () {
			var pal = new Palette(), result;

			result = pal.toRgb('#FFAA44');
			result.should.equal('rgb(255, 170, 68)');
		});

		it('throws ex if format of string is not ok', function () {
			var pal = new Palette();

			(function () {pal.toRgb('red');}).should.throw(ReferenceError);
		});

		it('throws ex if string has rubbish', function () {
			var pal = new Palette();

			(function () {pal.toRgb('33rgb(255, 170, 68)cc');}).should.throw(ReferenceError);
		});
	});

	describe('#toHex', function () {
		it('returns hex color from rgb', function () {
			var pal = new Palette(), result;

			result = pal.toHex('rgb(255, 170, 0)');
			result.should.equal('#ffaa00');
		});

		it('throws ex if format of string is not ok', function () {
			var pal = new Palette();

			(function () {pal.toHex('red');}).should.throw(ReferenceError);
		});

		it('throws ex if string has rubbish', function () {
			var pal = new Palette();

			(function () {pal.toHex('aass#ccdd22nxn');}).should.throw(ReferenceError);
		});

		it('throws ex if part of color is more than 255', function () {
			var pal = new Palette();

			(function () {pal.toHex('rgb(255, 256, 0)');}).should.throw(ReferenceError);
		});

		it('returns normal hex from rgb with spaces', function () {
			var pal = new Palette(), result;
			result = pal.toHex('rgb(  255  ,  170    ,  0     )');
			result.should.equal('#ffaa00');
		});

		it('returns normal hex from rgb without spaces', function () {
			var pal = new Palette(), result;
			result = pal.toHex('rgb(255,170,0)');
			result.should.equal('#ffaa00');
		});
	});

});