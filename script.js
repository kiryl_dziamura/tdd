window.addEventListener('load', function () {

	var pal = new Palette();
	var hexInput = document.getElementById('hex');
	var rgbInput = document.getElementById('rgb');
	var colBlock = document.getElementById('color');

	hexInput.addEventListener('change', function () {
		rgbInput.value = pal.toRgb(this.value);
		color.style.backgroundColor = pal.toRgb(this.value);
	});

	rgbInput.addEventListener('change', function () {
		hexInput.value = pal.toHex(this.value);
		color.style.backgroundColor = pal.toHex(this.value);
	});

});